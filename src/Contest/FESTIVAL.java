package Contest;

class FESTIVAL extends TiketKonser {//deklarasi kelas FESTIVAL bahwa FESTIVAL adalah subkelas dari kelas induk TiketKonser
    //Do your magic here...
    public FESTIVAL(String nama, double harga) {//deklarasi konstruktor untuk kelas FESTIVAL
        super(nama, harga);// memanggil konstruktor dari kelas TiketKonser
    }

    @Override
    public double hitungHarga() {//untuk mewarisi method dari kelas induk TiketKonser
        return harga;
    }
}
