package Contest;

abstract class TiketKonser implements HargaTiket {//deklarasi kelas abstrak TiketKonser yang mengimplementasikan interface HargaTiket
    // Do your magic here...
    protected String nama;//protected mengindikasikan bahwa variabel ini dapat diakses oleh kelas turunan.
    protected double harga;//protected mengindikasikan bahwa variabel ini dapat diakses oleh kelas turunan.

    public TiketKonser(String nama, double harga) {//konstruktor kelas TiketKonser
        this.nama = nama;
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public abstract double hitungHarga();
}