package Contest;

class VVIP extends TiketKonser {//deklarasi kelas VVIP bahwa VVIP adalah subkelas dari kelas induk TiketKonser
    // Do your magic here...
    public VVIP(String nama, double harga) {//deklarasi konstruktor untuk kelas VVIP
        super(nama, harga);// memanggil konstruktor dari kelas TiketKonser
    }

    @Override//untuk mewarisi method dari kelas induk TiketKonser
    public double hitungHarga() {
        return harga;
    }
}