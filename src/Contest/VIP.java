package Contest;

class VIP extends TiketKonser {//deklarasi kelas VIP bahwa VIP adalah subkelas dari kelas induk TiketKonser
    // Do your magic here...
    public VIP(String nama, double harga) {//deklarasi konstruktor untuk kelas VIP
        super(nama, harga);// memanggil konstruktor dari kelas TiketKonser
    }

    @Override//untuk mewarisi method dari kelas induk TiketKonser
    public double hitungHarga() {
        return harga;
    }
}