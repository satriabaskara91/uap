package Contest;

class CAT8 extends TiketKonser {//deklarasi kelas CAT8 bahwa CAT8 adalah subkelas dari kelas induk TiketKonser
    //Do your magic here...
    public CAT8(String nama, double harga) {//deklarasi konstruktor untuk kelas CAT8
        super(nama, harga);// memanggil konstruktor dari kelas TiketKonser
    }

    @Override//untuk mewarisi method dari kelas induk TiketKonser
    public double hitungHarga() {
        return harga;
    }
}
