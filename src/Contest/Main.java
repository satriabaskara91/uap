/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        //Do your magic here...
        Scanner scanner = new Scanner(System.in);

        System.out.println("Selamat datang di aplikasi Pemesanan Tiket Coldplay");
        //try digunakan untuk menghandle kemungkinan terjadinya pengecualian (exception) pada saat membaca input nama pemesan dari pengguna.

        try {
            System.out.print("Masukkan nama pemesan: ");
            String namaPemesan = scanner.nextLine();

            if (namaPemesan.length() > 10) {//Jika panjang namaPemesan lebih dari 10 karakter, maka akan mencetak "Panjang nama tidak boleh melebihi 10 karakter"
                throw new InvalidInputException("Panjang nama tidak boleh melebihi 10 karakter!");
            }

            System.out.println("Pilih jenis tiket:");
            System.out.println("1. CAT 8");
            System.out.println("2. CAT 1");
            System.out.println("3. FESTIVAL");
            System.out.println("4. VIP");
            System.out.println("5. UNLIMITED EXPERIENCE");
            System.out.print("Masukkan pilihan: ");
            //Menggunakan objek scanner untuk membaca input yang diberikan oleh pengguna dan menyimpannya dalam variabel "pemilihan".
            //Jika input yang diberikan tidak dapat diubah menjadi tipe data integer, maka akan terjadi pengecualian NumberFormatException.
            //Jika pengecualian NumberFormatException terjadi, baris selanjutnya akan melemparkan pengecualian InvalidInputException dengan pesan "Input tidak valid! Masukkan pilihan tiket dengan angka.".

            int choice;
            try {
                choice = Integer.parseInt(scanner.nextLine());//Menggunakan objek scanner untuk membaca input yang diberikan oleh pengguna dan menyimpannya dalam variabel "pemilihan".
            } catch (NumberFormatException e) {
                throw new InvalidInputException("Input tidak valid! Masukkan pilihan tiket dengan angka.");
            }
            //Jika nilai "pemilihan" kurang dari 1 atau lebih dari 5, maka kondisi if akan bernilai true.
            //Jika kondisi if bernilai true, baris selanjutnya akan melemparkan pengecualian InvalidInputException dengan pesan "Input tidak valid! hanya ada 1-5."

            if (choice < 1 || choice > 5) {
                throw new InvalidInputException("Input tidak valid! hanya ada 1-5.");
            }

            TiketKonser tiket = PemesananTiket.pilihTiket(choice - 1);

            String kodeBooking = generateKodeBooking();

            String tanggalPesanan = getCurrentDate();

            System.out.println("\n----- Detail Pemesanan -----");
            System.out.println("Nama Pemesan: " + namaPemesan);
            System.out.println("Kode Booking: " + kodeBooking);
            System.out.println("Tanggal Pesanan: " + tanggalPesanan);
            System.out.println("Tiket yang dipesan: " + tiket.getNama());
            System.out.println("Total harga: " + tiket.hitungHarga() + " USD");
        } catch (InvalidInputException e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }


    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}