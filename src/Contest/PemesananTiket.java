package Contest;

class PemesananTiket {
    // Do your magic here...
    private static TiketKonser[] tiketKonser;//Array dari tiketkonser

    static {
        tiketKonser = new TiketKonser[]{//menginisialisasi array tiketKonser dengan objek-objek TiketKonser
            new CAT8("CAT 8", 1000000),
            new CAT1("CAT 1", 4000000),
            new FESTIVAL("FESTIVAL", 6000000),
            new VIP("VIP", 8000000),
            new VVIP("UNLIMITED EXPERIENCE", 13000000)
        };
    }

    public static TiketKonser pilihTiket(int index) {//deklarasi metode pilihTiket untuk memilih dan mengembalikan objek TiketKonser berdasarkan indeks yang diberikan
        return tiketKonser[index];//mengembalikan objek TiketKonser dari array tiketKonser berdasarkan indeks yang diberikan
    }
}
