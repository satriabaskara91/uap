package Contest;

class CAT1 extends TiketKonser {//deklarasi kelas CAT1 bahwa CAT1 adalah subkelas dari kelas TiketKonser
    //Do your magic here...
     public CAT1(String nama, double harga) {//deklarasi konstruktor untuk kelas CAT1
        super(nama, harga);// memanggil konstruktor dari kelas TiketKonser
    }

    @Override//untuk mewarisi method dari kelas induk TiketKonser
    public double hitungHarga() {
        return harga;
    }
}
